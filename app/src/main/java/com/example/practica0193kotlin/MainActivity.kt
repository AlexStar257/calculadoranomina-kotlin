package com.example.practica0193kotlin

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast

class MainActivity : AppCompatActivity() {
    private lateinit var btnSaludar: Button
    private lateinit var btnLimpiar: Button
    private lateinit var btnCerrar: Button
    private lateinit var lblSaludar: TextView
    private lateinit var txtSaludo: EditText}
    override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_main)

    // Relación de los objetos de java con las views xml
    btnSaludar = findViewById(R.id.btnSaludar)
    btnLimpiar = findViewById(R.id.btnLimpiar)
    btnCerrar = findViewById(R.id.btnCerrar)
    lblSaludar = findViewById(R.id.lblSaludo)
    txtSaludo = findViewById(R.id.txtSaludo)

    // Codificar el evento click del boton Saludar
    btnSaludar.setOnClickListener {
        // Aqui se realiza la programacion
        if (txtSaludo.text.toString().isEmpty()) {
            // Falto capturar nombre
            Toast.makeText(this@MainActivity, "Favor de ingresar el nombre", Toast.LENGTH_SHORT).show()
        } else {
            val saludar = txtSaludo.text.toString()
            lblSaludar.text = "Hola $saludar Como estas?"
        }
    }

    // Codificar el evento click del boton limpiar
    btnLimpiar.setOnClickListener {
        // Aqui se realiza la programacion
        lblSaludar.text = ""
        txtSaludo.setText("")
        txtSaludo.requestFocus()
    }

    // Codificar el evento click del boton limpiar
    btnCerrar.setOnClickListener {
        // Aqui se realiza la programacion
        finish()
    }
}
}
